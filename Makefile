SHELL := /bin/bash

setup:
	( \
		python3 -m venv .venv ; \
		source .venv/bin/activate ; \
		python3 setup.py install ; \
	)
