"""Module for testing models
"""

import numpy as np
import pytest
from betsi.models import autoencoder, custom_autoencoder
from tensorflow.keras.datasets import mnist


def test_autoencoder():
    """Test happy case for autoencoder
    """
    n_hidden = 32

    (x_train, _), (x_test, _) = mnist.load_data()

    x_train = x_train.astype('float32') / 255.
    x_test = x_test.astype('float32') / 255.
    x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
    x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

    autoencoder_model, encoder_model, decoder_model = autoencoder(
        len(x_train[0]), n_hidden)
    autoencoder_model.compile(optimizer="adam", loss="binary_crossentropy")

    autoencoder_model.fit(x_train,
                          x_train,
                          epochs=2,
                          batch_size=256,
                          shuffle=True,
                          validation_data=(x_test, x_test))

    encoded_images = encoder_model.predict(x_test)
    decoded_images = decoder_model.predict(encoded_images)

    ae_decoded_images = autoencoder_model.predict(x_test)
    diff = ae_decoded_images - decoded_images

    assert np.count_nonzero(diff) == 0
    assert isinstance(decoded_images, (np.ndarray, np.generic))
    assert decoded_images.shape == x_test.shape
    assert encoded_images.shape[1] == n_hidden


def test_custom_autoencoder_happy():
    """Test happy case for custom autoencoder
    """
    (x_train, _), (x_test, _) = mnist.load_data()

    x_train = x_train.astype('float32') / 255.
    x_test = x_test.astype('float32') / 255.
    x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
    x_test = x_test.reshape((len(x_test), np.prod(x_test.shape[1:])))

    layer_dims = [len(x_train[0]), 128, 64, 32]

    autoencoder_model, encoder_model, decoder_model = custom_autoencoder(
        layer_dims)
    autoencoder_model.compile(optimizer="adam", loss="binary_crossentropy")

    autoencoder_model.fit(x_train,
                          x_train,
                          epochs=2,
                          batch_size=256,
                          shuffle=True,
                          validation_data=(x_test, x_test))

    encoded_images = encoder_model.predict(x_test)
    decoded_images = decoder_model.predict(encoded_images)
    ae_decoded_images = autoencoder_model.predict(x_test)

    diff = ae_decoded_images - decoded_images

    assert np.count_nonzero(diff) == 0
    assert isinstance(decoded_images, (np.ndarray, np.generic))
    assert decoded_images.shape == x_test.shape
    assert encoded_images.shape[1] == layer_dims[-1]


def test_custom_autoencoder_sad():
    """Test sad case for custom autoencoder
    """
    with pytest.raises(TypeError):
        _, _, _ = custom_autoencoder("abcd")

    with pytest.raises(TypeError):
        _, _, _ = custom_autoencoder([512, "bcd"])

    with pytest.raises(ValueError):
        _, _, _ = custom_autoencoder([1])

    with pytest.raises(ValueError):
        _, _, _ = custom_autoencoder([])

    with pytest.raises(ValueError):
        _, _, _ = custom_autoencoder(["3"])
