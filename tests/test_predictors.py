"""Module for testing predictors
"""

import numpy as np
import pytest
from betsi import predictors


def test_happy_distance_measure():
    """Test happy case for distance measure
    """

    array_1 = np.array([-1, 2, 3])
    array_2 = np.array([-2, 1, 2])

    dist = predictors.distance_measure(array_1, array_2)

    assert dist == pytest.approx(0.5169731539571706)


def test_sad_distance_measure():
    """Test sad case for distance measure
    """

    list_1 = [-1, 2, 3]
    list_2 = [-2, 1, 2]

    with pytest.raises(TypeError):
        _ = predictors.distance_measure(list_1, list_2)


def test_get_events():
    """Test for get_events
    """
    distance_list = [1, 2, 1]
    distance_value = 1

    events_1 = predictors.get_events(distance_list, 0)
    events_2 = predictors.get_events(distance_list, 300)

    assert events_1 == [1]
    assert events_2 == []

    with pytest.raises(TypeError):
        _ = predictors.get_events(distance_value, 2)
