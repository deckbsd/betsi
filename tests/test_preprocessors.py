"""Module for testing preprocessors
"""

import numpy as np
import pandas as pd
import pytest
from betsi import preprocessors


def test_normalize_all_data():
    """Test happy case for normalize_all_data
    """
    test_ip = pd.DataFrame.from_records([[1, -1, 2], [2, 0, 0], [0, 1, -1]])
    n_test_ip, _ = preprocessors.normalize_all_data(test_ip)

    test_arr = test_ip.to_numpy()
    n_test_arr, _ = preprocessors.normalize_all_data(test_arr)

    assert isinstance(n_test_ip, pd.DataFrame)
    assert isinstance(n_test_arr, np.ndarray)


@pytest.mark.parametrize("window_size, stride", [(2, -2), (2, 1)])
def test_happy_convert_to_from_column(window_size, stride):
    """Test happy case for convert_to_column
    """
    test_arr = pd.DataFrame(np.array([range(10), range(11, 21)]))

    column_df_1 = preprocessors.convert_to_column(test_arr, window_size,
                                                  stride)
    _ = preprocessors.convert_from_column(column_df_1, window_size, stride)


@pytest.mark.parametrize("window_size, stride", [(0, 2), (2, 0)])
def test_sad_convert_to_column(window_size, stride):
    """Test sad case for convert_to_column
    """
    test_arr = np.array([range(10), range(11, 21)])

    with pytest.raises(ValueError):
        _ = preprocessors.convert_to_column(test_arr, window_size, stride)
