# betsi

[![pipeline status](https://gitlab.com/deepchaos/betsi/badges/master/pipeline.svg)](https://gitlab.com/deepchaos/betsi/commits/master)
[![coverage report](https://gitlab.com/deepchaos/betsi/badges/master/coverage.svg)](https://gitlab.com/deepchaos/betsi/commits/master)

---

**B**ehaviour **E**xtraction for **T**ime-**S**eries **I**nvestigation using Deep Learning

Deeplearning module for event detection in time series through behavioural extraction.

---

## Project Structure

```bash
src/
    betsi/
        models.py
        preprocessors.py
tests/
    test_models.py
    test_preprocessors.py
```

---

## Installation (from source)

```bash
# Clone from source
$ git clone https://gitlab.com/deepchaos/betsi.git

# Switch to the directory
$ cd betsi

# Create and switch to a virtual environment
$ python3 -m venv .venv
$ source .venv/bin/activate

# To install a non-editable version
(.venv) $ python3 setup.py install

# To install an editable version
(.venv) $ python3 -m pip install -e .
```

---

## Usage

```python
(.venv) $ python3
>>> from betsi import models, preprocessors
>>> help(models) # To get the functions and parameters
>>> help(preprocessors)
```

---

## Reference

This project is based on the paper ‘Time Series Segmentation through Automatic Feature Learning’
by Lee, Wei-Han, et al.

Refer: ArXiv:1801.05394 [Cs, Stat], Jan. 2018. arXiv.org, <http://arxiv.org/abs/1801.05394>
